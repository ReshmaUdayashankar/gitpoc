package com.vinove.GitPOC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitPocApplication.class, args);
	}

}
